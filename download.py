import botocore
from boto3.session import Session

aws_access_key_id = 'minio'
aws_secret_access_key = 'miniostorage'

session = Session(aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
s3 = session.resource('s3', endpoint_url='http://10.121.240.233:9000')

for bucket in s3.buckets.all():
    print('bucket name:%s' % bucket.name)
