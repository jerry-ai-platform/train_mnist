# example on how to build docker:
# DOCKER_BUILDKIT=1 docker build -t benjamintanweihao/kubeflow-mnist . -f Dockerfile
# DOCKER_BUILDKIT=1 docker build --no-cache -t benjamintanweihao/kubeflow-mnist env -f Dockerfile

# example on how to run:
# docker run -it benjamintanweihao/kubeflow-mnist /bin/bash

FROM tensorflow/tensorflow:2.0.4-gpu-py3
LABEL MAINTAINER "Kerwin Tsai"
SHELL ["/bin/bash", "-c"]

# Set the locale
RUN  echo 'Acquire {http::Pipeline-Depth "0";};' >> /etc/apt/apt.conf
RUN DEBIAN_FRONTEND="noninteractive"
RUN apt-get update  && apt-get -y install --no-install-recommends locales && locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apt-get update && apt-get install -y --no-install-recommends \
    wget \
    git \
    unzip \
    python3-pip \
    openssh-client \
    python3-setuptools \
    google-perftools && \
    rm -rf /var/lib/apt/lists/*

# install conda
WORKDIR /tmp
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-4.7.12-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc

# build conda environments
COPY environment.yml /tmp/kubeflow-mnist/conda/
RUN /opt/conda/bin/conda update -n base -c defaults conda
RUN /opt/conda/bin/conda env create -f /tmp/kubeflow-mnist/conda/environment.yml
RUN /opt/conda/bin/conda clean -afy

# Install Minio Objecrt storage client
RUN wget --quiet https://dl.min.io/client/mc/release/linux-amd64/mc && \
    chmod +x mc && \
    mv mc /usr/bin 
RUN wget --quiet https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -O awscliv2.zip && \
    unzip awscliv2.zip && \
    ./aws/install
RUN aws configure set aws_access_key_id minio && \
    aws configure set aws_secret_access_key miniostorage && \
    aws configure set default.region us-west-2 && \ 
		alias aws='aws --endpoint-url http://10.121.240.233:9000'

# Get repo
RUN git clone https://gitlab.com/jerry-ai-platform/train_mnist.git /workspace

# Cleanup
RUN rm -rf /workspace/{nvidia,docker}-examples && rm -rf /usr/local/nvidia-examples && \
    rm /tmp/kubeflow-mnist/conda/environment.yml

# switch to the conda environment
RUN echo "conda activate kubeflow-mnist" >> ~/.bashrc
ENV PATH /opt/conda/envs/kubeflow-mnist/bin:$PATH
RUN /opt/conda/bin/activate kubeflow-mnist

# make /bin/sh symlink to bash instead of dash:
RUN echo "dash dash/sh boolean false" | debconf-set-selections && \
    DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

RUN touch /root/mount.sh && echo "modprobe fuse && s3fs -o passwd_file=/root/.passwd-s3fs -o url=http://10.121.240.233:9000 -o allow_other -o nonempty -o no_check_certificate -o use_path_request_style -o umask=000 mlpipeline /mnt" > /root/a.sh && chmod +x /root/a.sh
# Set the new Allocator
ENV LD_PRELOAD /usr/lib/x86_64-linux-gnu/libtcmalloc.so.4
RUN echo 'alias aws="aws --endpoint-url http://10.121.240.233:9000"' >> ~/.bashrc