import calendar
import os
import time
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow as tf
import pickle
import argparse

from constants import PROJECT_ROOT

aws_access_key_id = 'minio'
aws_secret_access_key = 'miniostorage'


def storage_helper_up(sourceDir: str, bucket_name: str):

    session = Session(aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

    s3 = session.resource('s3', endpoint_url='http://10.121.240.233:9000')

    uploadFileNames = []

    for (sourceDir, dirname, filename) in os.walk(sourceDir):
        uploadFileNames.extend(filename)
        break

    for filename in uploadFileNames:
        sourcepath = os.path.join(sourceDir + '/' + filename)
        destpath = 'save_model' + '/' + filename

        print ('Uploading %s to Amazon S3 bucket %s' %(sourcepath, bucket_name))

        filesize = os.path.getsize(sourcepath)
        print('Size: '+ str(filesize))

        s3.Bucket(bucket_name).upload_file(sourcepath, destpath)

def storage_helper_dow(sourceDir: str, p_name: str):

    session = Session(aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

    s3 = session.resource('s3', endpoint_url='http://10.121.240.233:9000')

    bucket = s3.Bucket(p_name)
    for obj in bucket.objects.filter(Prefix = sourceDir):
        if not os.path.exists(os.path.dirname(obj.key)):
            os.makedirs(os.path.dirname(obj.key))
        bucket.download_file(obj.key, obj.key)

def train(data_dir: str, batch_size: str, epoch: str, bucket_name: str):

    input_shape = (28, 28, 1)
    num_classes = 10
    # Training
    model = keras.Sequential(
        [
            keras.Input(shape=input_shape),
            layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Flatten(),
            layers.Dropout(0.5),
            layers.Dense(num_classes, activation="softmax")
        ]
    )
    model.summary()
    model.compile(optimizer='adam',loss="categorical_crossentropy",metrics=['accuracy'])

    # Download training dataset
    storage_helper_dow(data_dir,bucket_name)

    with open(os.path.join(data_dir, 'train_images.pickle'), 'rb') as f:
        train_images = pickle.load(f)

    with open(os.path.join(data_dir, 'train_labels.pickle'), 'rb') as f:
        train_labels = pickle.load(f)

    model.fit(train_images, train_labels, batch_size=int(batch_size), epochs=int(epoch), validation_split=0.1)

    with open(os.path.join(data_dir, 'test_images.pickle'), 'rb') as f:
        test_images = pickle.load(f)

    with open(os.path.join(data_dir, 'test_labels.pickle'), 'rb') as f:
        test_labels = pickle.load(f)

    # Evaluation
    test_loss, test_acc = model.evaluate(test_images, test_labels, verbose=2)

    print(f'Test Loss: {test_loss}')
    print(f'Test Acc: {test_acc}')

    # Save model
    if args.model_path:
        model_path = args.model_path
    else:
        model_path = PROJECT_ROOT

    ts = calendar.timegm(time.gmtime())
    model_path = os.path.join(model_path, str(ts))
    tf.saved_model.save(model, model_path)

    with open(os.path.join(PROJECT_ROOT, 'output.txt'), 'w') as f:
        f.write(model_path)
        print(f'Model written to: {model_path}')

    # Upload model and variable
    storage_helper_upload(model_path, bucket_name)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Kubeflow FMNIST training script')
    parser.add_argument('--data_dir', help='path to images and labels.')
    parser.add_argument('--model_path', help='folder to export model')
    parser.add_argument('--train-steps',help='train steps')
    parser.add_argument('--learning_rate',help='learning rate')
    parser.add_argument('--epoch',help='epoch')
    parser.add_argument('--batch-size',help='batch size')
    parser.add_argument('--bucket_name', help='buckut_name')
    args = parser.parse_args()

    train(data_dir=args.data_dir,batch_size=args.batch_size, epoch=args.epoch, bucket_name=args.bucket_name)
