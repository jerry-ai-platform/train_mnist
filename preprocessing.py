import argparse
import os
import pickle
import numpy as np
import botocore
import random
import string

from tensorflow import keras
from datetime import datetime
from boto3.session import Session


def storage_helper(sourceDir: str, bucket_name: str):
    aws_access_key_id = 'minio'
    aws_secret_access_key = 'miniostorage'

    session = Session(aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

    s3 = session.resource('s3', endpoint_url='http://10.121.240.233:9000')


    print("Create bucket: "+ bucket_name)
    s3.create_bucket(Bucket=bucket_name)

    uploadFileNames = []

    for (sourceDir, dirname, filename) in os.walk(sourceDir):
        uploadFileNames.extend(filename)
        break

    for filename in uploadFileNames:
        sourcepath = os.path.join(sourceDir + '/' + filename)
        destpath = 'dataset' + '/' + filename

        print ('Uploading %s to Amazon S3 bucket %s' %(sourcepath, bucket_name))

        filesize = os.path.getsize(sourcepath)
        print('Size: '+ str(filesize))

        s3.Bucket(bucket_name).upload_file(sourcepath, destpath)


def preprocess(data_dir: str, p_name: str):
    num_classes = 10

    fashion_mnist = keras.datasets.fashion_mnist
    (train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

    train_images = train_images.astype("float32")  / 255.0
    test_images = test_images.astype("float32")  / 255.0

    train_images = np.expand_dims(train_images, -1)
    test_images = np.expand_dims(test_images, -1)

    print("train_image shape:", train_images.shape)
    print(train_images.shape[0], "train samples")
    print(test_images.shape[0], "test samples")

    train_labels = keras.utils.to_categorical(train_labels, num_classes)
    test_labels = keras.utils.to_categorical(test_labels, num_classes)

    os.makedirs(data_dir, exist_ok=True)

    with open(os.path.join(data_dir, 'train_images.pickle'), 'wb') as f:
        pickle.dump(train_images, f)

    with open(os.path.join(data_dir, 'train_labels.pickle'), 'wb') as f:
        pickle.dump(train_labels, f)

    with open(os.path.join(data_dir, 'test_images.pickle'), 'wb') as f:
        pickle.dump(test_images, f)

    with open(os.path.join(data_dir, 'test_labels.pickle'), 'wb') as f:
        pickle.dump(test_labels, f)

    storage_helper(data_dir, bucket_name)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Kubeflow MNIST training script')
    parser.add_argument('--data_dir', help='Path to images and labels.')
    parser.add_argument('--bucket_name', help='buckut_name')
    args = parser.parse_args()

    preprocess(data_dir=args.data_dir, bucket_name=args.bucket_name)
